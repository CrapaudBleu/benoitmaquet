(function($) {

    "use strict";


    $(document).ready(function() {

      // ----- ServiceWorker
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/serviceworkers.js', {
        scope: '/'
      }).then(function(reg) {
        // registration worked
        console.log('Registration succeeded. Scope is ' + reg.scope);
      }).catch(function(error) {
        // registration failed
        console.log('Registration failed with ' + error);
      });

      window.addEventListener("beforeinstallprompt", function(e) {
        // log the platforms provided as options in an install prompt
        console.log(e.platforms); // e.g., ["web", "android", "windows"]
        e.userChoice.then(function(outcome) {
          console.log(outcome); // either "accepted" or "dismissed"
        }, handleError);
        console.log(e);
        e.preventDefault();
      });
    }

		// PRELOADER
        $("body").toggleClass("loaded");
        setTimeout(function() {
            $("body").addClass("loaded");
        }, 3000);

		// PORTFOLIO DIRECTION AWARE HOVER EFFECT
		var item = $("#bl-work-items>div");
		var elementsLength = item.length;
		for (var i = 0; i < elementsLength; i++) {
			$(item[i]).hoverdir();
		}

		// TEXT ROTATOR
		$("#selector").animatedHeadline({
             animationType: "clip"
        });

		// BOX LAYOUT
        Boxlayout.init();

		// REMOVE # FROM URL
		$("a[href='#']").on("click", (function(e) {
			e.preventDefault();
		}));



		// MATERIAL CAROUSEL
        $(".carousel.carousel-slider").carousel({
            fullWidth: true,
            indicators: true,
        });

		// RESUME CARDS ANIMATION
		if ($(window).width() > 800) {
			$(".resume-list-item, .resume-card").on("click", function() {
				$(".resume-list-item").removeClass("is-active");
				var e = parseInt($(this).data("index"),10);
				$("#resume-list-item-" + e).addClass("is-active");
				var t = e + 1,
					n = e - 1,
					i = e - 2;
				$(".resume-card").removeClass("front back up-front up-up-front back-back"), $(".resume-card-" + e).addClass("front"), $(".resume-card-" + t).addClass("back"), $(".resume-card-" + n).addClass("back-back"), $(".resume-card-" + i).addClass("back")
			});
		}

    });

})(jQuery);
