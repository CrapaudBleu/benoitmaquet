const cacheName = `AntoinePicquout`;


self.addEventListener('install', e => {
	e.waitUntil(
		caches.open(cacheName).then(cache => {
			return cache.addAll([
					`/`,
				])
				.then(() => self.skipWaiting());
		})
	);
});

self.addEventListener('activate', event => {
	event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', function (evt) {
	// console.log('The service worker intercept the following network\'s request : ', evt.request.url);
	evt.respondWith(
		fromNetwork(evt.request, 3000)
		.catch(function () {
			return fromCache(evt.request);
		})
	);
});

/**
 * Handle a request and try it if return error or delay it's too big return cache
 */
function fromNetwork(request, timeout) {
	return new Promise(function (fulfill, reject) {
		var timeoutID = setTimeout(reject, timeout);

		self.fetch(request).then(function (response) {
			// console.log('Network response received', response);
			clearTimeout(timeoutID);
			fulfill(response);
		}, reject);
	});
}

/**
 * Handle a request and try to return a cache response
 */
function fromCache(request) {
	// console.log('REQUEST THE CACHE');
	return caches.open(cacheName).then(function (cache) {
		// console.log(request);
		return cache.match(request).then(function (matching) {
			// console.log('matching', matching);
			return matching || Promise.reject('no match');
		});
	});
}
